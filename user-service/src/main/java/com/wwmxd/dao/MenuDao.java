package com.wwmxd.dao;

import com.wwmxd.entity.Menu;
import com.wwmxd.common.mapper.SuperMapper;

import java.util.List;

/**
 *
 * 
 *
 * @author WWMXD
 * @email 309980030@qq.com
 * @date 2018-01-03 14:39:38
 */
public interface MenuDao extends SuperMapper<Menu> {
}